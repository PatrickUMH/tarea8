#include <stdio.h>
int main(int argc, char const *argv[]) {

  printf("Tamaño de un int en bits: %d \n", sizeof(int) );
  printf("Tamaño de un float en bits: %d \n", sizeof(float) );
  printf("Tamaño de un char en bits: %d \n", sizeof(char) );
  printf("Tamaño de un long int en bits: %d \n",sizeof(long int) );

  return 0;
}
